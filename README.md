# Specs

This project is about new features that people want in Abantu. Every new feature should have a folder, properly named, and a markdown file with at least a title , the name of the feature, and a brief description. It should be commit on a separate branch and a merge request must be submitted.

When the merge request is finally approved a issue must be created on gitlab to mirror that spec and log its development.
